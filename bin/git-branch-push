#! /bin/sh

usage() {
    cat << EOF
Usage:
    git branch-push [<remote-branch>]
        Pushes HEAD to the <remote-branch>. If <remote-branch> is omitted,
        it defaults to the current branch's upstream. Either way, <remote
        -branch> must exist.

    git branch-push --prompt [<remote-branch>]
        The same as above, but after the list of changes to push is shown,
        the user is prompted as to whether they wish to continue with the
        push. Mostly useful for previewing unpushed work.

    git branch-push --force <remote-branch>
        Force pushes HEAD to the <remote-branch>. <remote-branch> must be
        specified and must exist.

    git branch-push --create <remote-branch>
        Creates <remote-branch> on the remote and pushes HEAD to it.

    git branch-push --delete <remote-branch>
        Deletes <remote-branch> on the remote.
EOF
    exit $1
}

fail() {
    echo $@ 1>&2
    exit 1
}

case "$1" in
--prompt)
    promptMode='true'
    shift;
    ;;
--force)
    forceMode='true'
    promptMode='true'
    shift
    ;;
--create|-c)
    createMode='true'
    shift
    ;;
--delete|-d)
    deleteMode='true'
    promptMode='true'
    shift
    ;;
--help|-h)
    usage 0
    ;;
-*)
    echo "Unrecognized option: \"$1\"" 1>&2
    usage 1
esac

case "$1" in
-*)
    fail 'Only one option may be given at a time.'
esac

if [ -z "$1" ]; then
    if [ "$forceMode" = 'true' -o "$createMode" = 'true' -o "$deleteMode" = 'true' ]; then
        usage 1
    fi
    remoteBranch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2> /dev/null)
    if [ $? -ne 0 ]; then
        fail "Unable to find an upstream branch for \"$(git rev-parse --abbrev-ref HEAD)\"."
    fi
else
    remoteBranch=$1
fi

if [ -n "$2" ]; then
    usage 1
fi

remote=$(echo $remoteBranch | cut -d'/' -f1)
branch=$(echo $remoteBranch | sed "s:^$remote/::")

if [ "$remote/$branch" != "$remoteBranch" ]; then
    fail "\"$remoteBranch\" isn't in the format \"<remote>/<branch>\""
fi

if [ "$(git remote 2> /dev/null 2> /dev/null | grep $remote)" != "$remote"  ]; then
    fail "\"$remote\" doesn't appear to be a valid remote."
fi

if git show-branch $remoteBranch > /dev/null 2>&1; then
    remoteBranchExists='true'
fi

if [ "$createMode" = 'true' -a "$remoteBranchExists" = 'true' ]; then
    fail "Remote branch \"$remoteBranch\" already exists."
elif [ "$createMode" != 'true' -a "$remoteBranchExists" != 'true' ]; then
    fail "Remote branch \"$remoteBranch\" does not exist."
fi

if [ "$createMode" != 'true' -a "$deleteMode" != 'true' ]; then
    headHash="$(git rev-parse HEAD)"
    remoteHash="$(git rev-parse $remoteBranch)"
    if [ "$headHash" = "$remoteHash" ]; then
        echo 'Nothing to push.'
        exit 0
    fi

    echo 'Commits to push:'
    git --no-pager log --reverse --format=format:'%C(magenta)%ar %C(green)%an%Creset %s' $remoteHash..$headHash
    echo
    echo
    echo 'Files changed:'
    git --no-pager diff --stat $remoteHash..$headHash
    echo
fi

if [ "$promptMode" == 'true' ]; then
    echo -e "Destination branch: $(tput setaf 2)$remoteBranch$(tput sgr 0)"
    echo
    echo -n 'Continue with push? (y/N) '
    read -n 1 answer
    echo
    if [ "$answer" != 'y' -a "$answer" != 'Y' ]; then
        exit 0
    fi
    echo
fi

if [ "$deleteMode" = 'true' ]; then
    git push $remote --delete $branch
elif [ "$forceMode" = 'true' ]; then
    git push --force $remote HEAD:$branch
else
    git push $remote HEAD:$branch
fi

exit $?
